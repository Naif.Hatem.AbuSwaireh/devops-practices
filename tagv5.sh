#!/bin/bash

name="nabuswaireh/devops-practices"
#This is the name of the Docker Image.

tag=$(ls target/*.jar | cut -d - -f 2,3 | cut -d . -f 1)
#This is the tag of the Image (yymmdd-commitID) format.

db_type_is="h2"
#Replace the type of the database to be linked inside the quotations.

data_base="-Dspring.profiles.active=$db_type_is"
#This is the full command to run the DB.
#Replace the value of data_base to an empty String (= "") before running the docker-compose.yaml file.

jar="assignment-${tag}.jar"
#This is the selected .jar file dynamic name.


sudo docker build -t $name:$tag --build-arg DB_Val=${data_base} --build-arg jar_Val=${jar} .
#This command builds the Docker Image.
