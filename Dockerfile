FROM openjdk:8-jdk-alpine
#Base Image

ARG DB_Val=${a}
ARG jar_Val=${b}
#Recieve the arguments from the tagv5.sh file.


ENV JAR_FILE=${jar_Val}
#The full .jar file name is taken from the tagv5.sh file.

ENV DB_TYPE=${DB_Val}
#The DB type is based on the selected DB in the tagv5.sh file.

COPY ./target/*.jar  /opt
#Copy the .jar file to /opt directory inside the Docker Container.

WORKDIR /opt
#Make /opt as the Working Directory inside the Docker Container.


EXPOSE 8090

CMD java -jar ${DB_TYPE} ${JAR_FILE}
#Run this command once running a Docker Container.

